# Notes Demo PHP


# Prerequisites
 - PHP 7.1
 - Maria DB
 - Codeigniter 3.x 

# Variables
Copy file .env.example to .env and update DB variables

# Installing
Run docker in root source folder.
```
$ docker-compose up -d
```

Access docker container
```
$ docker exec -ti notesdemo_api_1 bash
```

After access to docker container

Run Composer install
```
$ composer install
```

Run Migrate data
```
$ composer migrate
```
==> You can check on browser with address : http://localhost:12000

Another, For check style code Please run Lint fix code
```
$ composer lint-fix
```
