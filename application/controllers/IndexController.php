<?php

class IndexController extends \Note\core\BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('note');
    }

    /**
     * Index - get list notes
     * @return mixed
     */
    public function index()
    {
        $data['title'] = "Dashboard";
        $data['notes'] = $this->note->findAll();
        return $this->load->template('index/index', $data);
    }

    /**
     * Create new note
     * @return mixed
     */
    public function create()
    {
        if ($this->input->post()) {
            try {
                $note = $this->input->post();
                if ($this->form_validation->run('notes')) {
                    $id = $this->note->insert($note);
                    $result = [
                        "status" => true,
                        "id" => $id,
                        "message" => "Create new note success"
                    ];
                } else {
                    $result = [
                        "status" => false,
                        "message" => validation_errors()
                    ];
                }
            } catch (Exception $e) {
                $result = [
                    "status" => false,
                    "message" => "Create new note fail"
                ];
                log_message("error", $e->getMessage());
            }
        }
        echo json_encode($result);
    }


    /**
     * Create new note
     * @return mixed
     */
    public function edit($id)
    {

        $data = $this->note->findById($id);
        if (empty($data)) {
            echo json_encode([
                "status" => false,
                "message" => "Note no available"
            ]);
        } else if ($this->input->post()) {
            try {
                $note = $this->input->post();
                if ($this->form_validation->run('notes')) {
                    $this->note->update($id, $note);
                    $result = [
                        "status" => true,
                        "message" => "Update note success"
                    ];
                } else {
                    $result = [
                        "status" => false,
                        "message" => validation_errors()
                    ];
                }
            } catch (Exception $e) {
                $result = [
                    "status" => false,
                    "message" => "Update note fail"
                ];
                log_message("error", $e->getMessage());
            }
            echo json_encode($result);
        }
    }


    /**
     * Delete note
     */
    public function delete()
    {
        if ($this->input->post()) {
            try {
                $id = $this->input->post("delete");
                if (is_numeric($id)) {
                    $this->note->delete($this->input->post("delete"));
                    $this->session->set_flashdata('success', 'Delete note success');
                } else {
                    $this->session->set_flashdata('error', 'Delete note fail, Please check again');
                }
            } catch (\Exception $e) {
                $this->session->set_flashdata('error', 'Delete note fail, Please check again');
                log_message("error", $e->getMessage());
            }
            redirect("/");
        }
    }
}
