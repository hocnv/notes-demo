<?php
use Note\core\MyModel;

class Note extends MyModel
{
    /**
     * Table name.
     */
    public $table = 'notes';

    public function __construct()
    {
        parent::__construct();
        $this->setTable($this->table);
    }
}
