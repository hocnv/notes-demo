<?php
namespace Note\core;
/**
 * CodeIgniter Rest Controller
 * A fully RESTful server implementation for CodeIgniter using one library, one config file and one controller.
 *
 * @category        Libraries
 * @version         3.0.0
 * @package         CodeIgniter
 * @subpackage      Libraries
 *
 * @license         MIT
 */
abstract class BaseController extends \CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library(['form_validation']);
        $this->load->helper(['pagination', 'template', 'common']);


    }

}
