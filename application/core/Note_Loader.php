<?php
/**
 * Template loading function for CodeIgniter 3.x
 * Location: application/core/JINJER_Loader.php
 * Class JINJER_Loader
 */
class Note_Loader extends CI_Loader
{
    public function template($template_name, $vars = array(), $return = false)
    {
        if ($return) {
            $content  = $this->view('templates/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('templates/footer', $vars, $return);
            return $content;
        } else {
            $this->view('templates/header', $vars);
            $this->view($template_name, $vars);
            $this->view('templates/footer', $vars);
        }
    }

    public function blanktmp($template_name, $vars = array(), $return = false)
    {
        if ($return) {
            $content  = $this->view('templates/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('templates/footer', $vars, $return);
            return $content;
        } else {
            $this->view('templates/header', $vars);
            $this->view($template_name, $vars);
            $this->view('templates/footer', $vars);
        }
    }
}
