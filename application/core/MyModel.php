<?php
namespace Note\core;

class MyModel extends \CI_Model
{
    //property
    private $table;

     /**
     * Primary key
     */
    public $primaryKey = 'id';

    /**
     *
     * テーブル名をセットする
     *
     *
     * @param string $name
     */
    public function setTable($name)
    {
        $this->table = $name;
    }

    /**
     *
     * 保存を行う(insert | update 自動判別）
     * @param array $data
     * @return boolean
     */
    public function save(array $data)
    {
        if (empty($data['id'])) {
            return $this->insert($data);
        } elseif ($this->exists($data['id'])) {
            return $this->update($data['id'], $data);
        } else {
            return false;
        }
    }

    /**
     * insert
     *
     * @return mixed integer | false
     *
     */
    public function insert(array $data)
    {
        $now = $this->now();
        
        if ($this->getSetColumn("created", $data) == false) {
            $this->db->set(array('created' => $now ));
        }
        if ($this->getSetColumn("updated", $data) == false) {
            $this->db->set(array('updated' => $now ));
        }
        $ret = $this->db->insert($this->table, $data);
        if (!$ret) {
            throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
        }
        return $this->db->insert_id();
    }

    /**
     * update
     *
     * @param integer $id
     * @param array $data
     * @return boolean
     */
    public function update($id, $data = null)
    {
        if ($data === null || empty($id)) {
            return false;
        }

        if (!array_key_exists('updated', $data)) {
            $data['updated'] = $this->now();
        }
        $ret = $this->db->update($this->table, $data, array('id' =>(INT) $id));
        if (!$ret) {
            throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
        }
        return $ret;
    }


    /**
     * multi update
     *
     * @param array $data
     * @param string $condition
     * @return Number of rows updated or FALSE on failure
     */
    public function updateBatch($data = null, $condition = 'id')
    {
        if ($data === null) {
            return false;
        }

        $ret = $this->db->update_batch($this->table, $data, $condition);
        if (!$ret) {
            throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
        }
        return $ret;
    }

    /**
     * multi insert
     *
     * @param array $data
     * @param string $condition
     * @return Number of rows inserted or FALSE on failure
     */
    public function insertBatch($data = null)
    {
        if ($data === null) {
            return false;
        }
        $ret = $this->db->insert_batch($this->table, $data);
        if (!$ret) {
            throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
        }
        return $ret;
    }

    /**
     * delete
     *
     * @param integer $id
     * @return boolean
     */
    public function delete($id)
    {
        if(!empty($id)) {
            $data['deleted'] = $this->now();
            $ret = $this->db->update($this->table, $data, array('id' =>(INT) $id));
            if (!$ret) {
                throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
            }
            if ($this->db->affected_rows() > 0 ) {
                return $ret;
            }
        }
        return false;
    }

    /**
     *
     * データの存在チェック
     *
     * @param integer $id
     */
    public function exists($id)
    {
        $data = $this->findById($id);
        if (!empty($data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Find records in database by id
     *
     * @param  integer id
     * @return array
     */
    public function findById($id)
    {
        if (!empty($id)) {
            $this->db->from($this->table);
            $this->db->where("id", $id);
            $this->db->where("deleted", null);
            $query = $this->db->get();
            $data = [];
            if ($query !== false && $query->num_rows() > 0) {
                $data = $query->row();
            }

            return $data;
        }
        return false;
    }

    public function findByConditions($where)
    {
        if(is_array($where) && !empty($where)){
            $query = $this->db->from($this->table)
                            ->where($where)
                            ->get();
            return $query->num_rows() > 0 ? $query->result_array():[];
        }

        return [];
    }

    public function updateByConditions($data, $where = null)
    {
        if(is_array($where) && !empty($where)){
            if (!array_key_exists('updated', $data)) {
                $data['updated'] = $this->now();
            }
            $ret = $this->db->update($this->table, $data, $where);
            return $ret;
        }
        return false;
    }


    /**
     * Find all record
     *
     * @return array results
     */
    public function findAll()
    {
        $models = $this->db->get_where($this->table, array("deleted" => null ));
        if (!$models) {
            return [];
        }
        return $models->result_array();
    }

    /**
     * Count all data of table
     * @param array $condition | null
     * @return array    Results
     */
    public function count($condition = null)
    {
        if ($condition != null) {
            $this->db->where($condition);
        }
        $this->db->where("deleted", null);
        return $this->db->count_all_results($this->table);
    }

    /**
     * now
     *
     * @return string
     */
    public function now()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * getSetColumn
     * @param string $field
     * @param array $data
     * @return boolean
     */
    public function getSetColumn($field, $data)
    {
        if (array_key_exists($field, $data)) {
            return true;
        }
        return false;
    }

    /**
     * create data
     * @param boolean $multiple
     * @param array $data
     * @return array
     */
    public function create($data, $multiple = false)
    {
        
        if ($multiple) {
            return $this->db->insert_batch($this->table, $data);
        }

        $result = $this->db->insert($this->table, $data);
        if (!$result) {
            throw new \Exception($this->db->error()['message'],$this->db->error()['code']);
        }

        if (!$multiple) {
            $data[$this->primaryKey] = $this->db->insert_id();
        }
        
        return $data;
    }
}
