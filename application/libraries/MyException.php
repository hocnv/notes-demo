<?php
class MyException extends \CI_Exceptions
{
    public function errorMessage(){
	    $errorMsg = 'Error on line '.$this->getLine().' in '.$this->getFile()
	    			.'. Message code: '.$this->getCode().' message: '.$this->getMessage();
	    return $errorMsg;
  	}
}
