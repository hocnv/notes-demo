  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Notes Talking
      </h1>
      <ol class="breadcrumb">
        <li><a class="create_note"><i class="fa fa-plus"></i> Create Note </a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-12">
          <?php
          showMessages($this->session->flashdata('success'), $this->session->flashdata('error'));
          ?>
        </div>

      </div>
      <div class="row note_list">
      <?php
        if (!empty($notes)) {
          foreach ($notes as $k =>$note) {
            $box_class = ($k%2 == 0 ) ? "box_class_even" : "box_class_odd";
            ?>
            <div class="col-md-3 item_note note_<?php echo $note['id']  ?>">
                <textarea readonly style="background-color: <?php echo $note['color'] ?>" class="box_note <?php echo $box_class ?>"><?php echo htmlentities($note['content']) ?></textarea>
                <a class="edit_note " data-color="<?php echo $note['color'] ?>" data-id="<?php echo $note['id'] ?>"> <i class="fa fa-edit"></i> </a>
                <a class="delete_note " data-id="<?php echo $note['id'] ?>"> <i class="fa fa-trash"></i> </a>
            </div>
      <?php
          }
        } else {
            echo "<div class='no_notes'>No notes available now.</div>";
        }

      ?>
      </div>
    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="detail_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="header_popup">Detail Note</h4>
        </div>
        <div class="modal-body">
          <div id="msg_result"></div>
          <?php echo form_open('/edit', ['id' => "form_notes"])?>
          <div class="form-group">
            <label class='required'>Content Note</label>
            <textarea class="form-control content_note" required name="content" rows="10"></textarea>
          </div>

          <div class="form-group">
            <label class='required'>Color</label>
            <select class="form-control color_note" name="color" required>
              <?php
              $colors = ["Red" => '#ff000082', "Blue" => '#0000ff85', "Yellow" => '#ffff0070', "Purple" => '#80008085',
                  "Brown" => '#a52a2a80', "Pink" => "#ffc0cb9e", "Green" => "#0080008c", "Gray" => "#8080807a"];
              foreach ($colors as $key => $color) {
                echo "<option value='$color'> $key</option>";
              }
              ?>
            </select>
          </div>
          <input type="hidden" class="id_note" name="id" value="" />

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success btn-save-notes btn-flat">Save</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="delete_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel">Delete Note</h4>
        </div>
        <div class="modal-body">
          <p>
            Are you sure for delete this note?
          </p>
          <?php echo form_open('/delete',['id' => 'delete_form'])?>
          <input type="hidden" class="delete_input" name="delete" value=""/>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger btn-delete-confirm btn-flat">Delete</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


