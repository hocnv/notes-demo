<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 10/14/18
 * Time: 15:11
 */

/**
 * Set data helper.
 *
 * Check and set data in view.
 * @param array $data
 * @param string $name
 * @param string $array_name
 * @param string $type
 * @param string $default
 *
 */
if (!function_exists("setData")) {
    function setData($data, $name, $array_name = "", $type = "t", $default = 0)
    {
        $dataName = (empty($array_name)?$name: $array_name);
        switch ($type) {
            case "radio":
                if (isset($data->$name)) {
                    echo set_radio($dataName, $default, ($data->$name == $default) ? true : false);
                } else {
                    echo set_radio($dataName, $default);
                }
                break;
            case "select":
                if (isset($data->$name)) {
                    echo set_select($dataName, $default, ($data->$name == $default) ? true : false);
                } else {
                    echo set_select($dataName, $default);
                }
                break;
            default:
                if (isset($data->$name)) {
                    echo set_value($dataName, $data->$name);
                } elseif ($default != 0) {
                    echo set_value($dataName, $default);
                } else {
                    echo set_value($dataName);
                }
        }
    }
}


/**
 * Show message helper.
 *
 * @param string $success
 * @param string $error
 *
 */
if (!function_exists('showMessages')) {
    function showMessages($success, $error, $warning = null)
    {
        $ci = & get_instance();
        $ci->session->set_flashdata('error', null);
        $ci->session->set_flashdata('success', null);
        $ci->session->set_flashdata('warning', null);

        if ($success) {
            $msg = $success;
            $class = "alert-success";
        } elseif ($error) {
            $msg = $error;
            $class = "alert-danger";
        } elseif ($warning) {
            $msg = '<i class="icon fa fa-warning "></i>'.$warning;
            $class = "alert-warning";
        }
        if (!empty($msg)) {
            $message = '<div class="alert '.$class.' alert-dismissable">';
            $message.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
            $message.= $msg.'</div>';
            echo $message;
        }
    }
}
