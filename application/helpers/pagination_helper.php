<?php
if (!function_exists("pagination")) {
    function pagination($url, $rowscount, $per_page, $uriSegment = 3)
    {
        $ci = & get_instance();
        $ci->load->library('pagination');
        
        $config = array();
        //if(count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['reuse_query_string'] = true;
        $config["base_url"] = $url;
        $config["total_rows"] = $rowscount;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $uriSegment;
        if (count($_GET) > 0) {
            $config['first_url'] = $config['base_url'].'/1/'.'?'.http_build_query($_GET);
        } else {
            $config['first_url'] = 1;
        }

        $config['use_page_numbers']  = true;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        // opening tag for current page link
        $config['cur_tag_open'] = '<li class="paginate_button active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        
        // customizing first link
        $config['first_link'] = '先頭';
        $config['first_tag_open'] = '<li class="paginate_button">';
        $config['first_tag_close'] = '</li>';

        // customize last link
        $config['last_link'] = '最後';
        $config['last_tag_open'] = '<li class="paginate_button">';
        $config['last_tag_close'] = '</li>';

        // customizing previous link
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '<';
        // customizing next link
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '>';

        // tag for all pages link
        $config['num_tag_open'] = '<li class="paginate_button">';
        $config['num_tag_close'] = '</li>';
        $ci->pagination->initialize($config);
        $pagination =  $ci->pagination->create_links();
        // get statistic pagination

        $fromPage = ($ci->pagination->cur_page -1) * $ci->pagination->per_page +1 ;
        $toPage =  $ci->pagination->cur_page * $ci->pagination->per_page;
        if ($toPage > $rowscount) {
            $toPage = $rowscount;
        }
        $statisticPagination =  "";
        if ($rowscount > $ci->pagination->per_page) {
            $statisticPagination =  "Showing from ".$fromPage." to ".$toPage." of ". $rowscount." entries";
        }

        return array("pagination" => $pagination, "statistic" => $statisticPagination);
    }
}
