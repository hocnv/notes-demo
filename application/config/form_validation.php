<?php
$config = [
	'notes' => [
        [
            'field' => 'content',
            'label' => 'Content Note',
            'rules' => 'trim|required',
            'errors' => [
                'required' => 'Please input content note.',
            ]
        ],
        [
            'field' => 'color',
            'label' => 'Color Note',
            'rules' => 'trim|required',
            'errors' => [
                'required' => 'Please choose color note.',
            ]
        ],
    ]
];
