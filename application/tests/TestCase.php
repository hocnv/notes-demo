<?php

class TestCase extends CIPHPUnitTestCase
{
	public $CI;

	public function __construct()
	{
		$this->CI = &get_instance();
		parent::__construct();
		$_SERVER['REMOTE_ADDR'] = '192.168.2.2';
		DbFactory::getFM()->create('Ip',[
			"ip_address" => $_SERVER['REMOTE_ADDR'],
		]);
	}

    /**
     * SetUp
     */
	public function setUp()
	{
		$this->request->enableHooks();
	}

	/**
	 * Custom assert Equals Status Code
	 * @param  integer $code
	 */
	public function assertStatusCode($code)
	{
		$this->assertEquals($code, $this->CI->json->getStatusCode());
	}

	/**
	 * Custom assert Equals Session Flash
	 * @param  string $msg
	 * @param  string $key
	 */
	public function assertSessionFlash($msg, $key)
	{
		$result =  $this->CI->session->flashdata($key);
		$this->assertEquals($msg, $result);
	}

}

