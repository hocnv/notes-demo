<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Agreement')->setDefinitions([
	'company_id' => 'factory|Company',
	'user_id' => 'factory|User',
	'type' => 0,
	'start_date' => Faker::date('Y-m-d', 'now'),
	'end_date' => Faker::date('Y-m-d', 'now'),
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
	'number' => 0,
	'first_contract' => 1
]);

