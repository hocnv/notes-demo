<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Role')->setDefinitions([

    'name' => Faker::name(),
    'setting' => '000000000000',
    'created' => Faker::date('Y-m-d H:i:s', 'now'),
    'updated' => Faker::date('Y-m-d H:i:s', 'now')
]);
