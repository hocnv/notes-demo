<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('AgencyPersonnel')->setDefinitions([
	'agency_id' => 'factory|Agency',
	'name' => Faker::name(),
	'furigana' => Faker::lastName(),
	'email' => Faker::unique()->email(),
	'tel' => '12-12-1234',
	'representative' => 1,
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
]);
