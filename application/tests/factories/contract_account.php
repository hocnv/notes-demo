<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('ContractAccount')->setDefinitions([
	'company_id' => 'factory|Company',
	'user_id' => 'factory|User',
	'apply_date' => Faker::date('Y-m-d', 'now'),
	'number' => 5,
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null
]);

