<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Plan')->setDefinitions([
	'company_id' => 'factory|Company',
	'start_date' => Faker::date('Y-m-d', 'now'),
	'jinji' => 0,
	'kintai' => 0,
	'roumu' => 0,
	'keihi' => 0,  
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
]);
