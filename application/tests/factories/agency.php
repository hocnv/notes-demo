<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Agency')->setDefinitions([
	'user_id' => 'factory|User',
	'name' => Faker::name(),
	'tel' => '0935088722',
	'zip' => Faker::postcode(),
	'address' => Faker::streetAddress(),
	'address2' => Faker::streetAddress(), 
	'contract_start_date' => Faker::date('Y-m-d', 'now'),
	'contract_end_date' => Faker::date('Y-m-d', 'now'),
	'contract_detail' => Faker::text(100),
	'customer_code' => Faker::postcode(),
	'use_start_date' => Faker::date('Y-m-d', 'now'), 
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
	'status' => 1
]);
