<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('OptionPlan')->setDefinitions([
	'plan_id' => 'factory|Plan',
	'option_id' => rand(1,9),
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
]);