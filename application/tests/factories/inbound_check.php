<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('InboundCheck')->setDefinitions([
    'user_id' => 'factory|User',
    'division_id' => 'factory|Division',
    'approval' => 1,
    'apply_date' => Faker::date('Y-m-d', 'now'),
    'number' => 1,
    'created' => Faker::date('Y-m-d H:i:s', 'now'),
    'updated' => Faker::date('Y-m-d H:i:s', 'now')
]);
