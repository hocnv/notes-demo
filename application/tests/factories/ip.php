<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Ip')->setDefinitions([
	'ip_address' => '192.168.2.2', 
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,

]);
