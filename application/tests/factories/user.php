<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('User')->setDefinitions([
    'name' => Faker::name(),
    'furigana' => Faker::lastName(),
    'division_id' => 'factory|Division',
    'role_id' => 'factory|Role',
    'mail_address' => Faker::unique()->email(),
    'created' => Faker::date('Y-m-d H:i:s', 'now'),
    'updated' => Faker::date('Y-m-d H:i:s', 'now')
]);

