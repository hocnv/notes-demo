<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('CompanySale')->setDefinitions([
	'division_id' => 'factory|Division',
	'create_user_id' => 'factory|User',
	'sales_user_id' => 'factory|User',
	'token' => Faker::sha256(),
	'expiration_date' => Faker::date('Y-m-d', 'now'), 
	'revocation' => 1,
	'use_start_date' => Faker::date('Y-m-d', 'now'), 
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
	'company_name' => Faker::name(),
	'application_name' => Faker::name(),
	'application_furigana' => Faker::name(),
	'application_mail_address' => Faker::unique()->email(),
	'note' => Faker::text(),
	'document_name' => 'test.pdf',
	'document_path' => 'uploads',
    'department_id' => 'factory|Department',
]);
