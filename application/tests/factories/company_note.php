<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('CompanyNote')->setDefinitions([
	'company_id' => 'factory|Company',
	'user_id' => 'factory|User',
	'actor' => 0,
	'note' => Faker::text(),
	'created' => Faker::date('Y-m-d H:i:s', 'now'),
	'updated' => Faker::date('Y-m-d H:i:s', 'now') ,
	'deleted' => null,
]);