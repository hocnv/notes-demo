<?php

use League\FactoryMuffin\Faker\Facade as Faker;

\DbFactory::getFM()->define('Department')->setDefinitions([
    'name' => Faker::unique()->name(),
    'status' => 1,
    'created' => Faker::date('Y-m-d H:i:s', 'now'),
    'updated' => Faker::date('Y-m-d H:i:s', 'now')
]);
