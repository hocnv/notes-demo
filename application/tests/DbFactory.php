<?php

use League\FactoryMuffin\FactoryMuffin;

class DbFactory
{
    protected static $fm;
    protected static $instance;

    public static function getFM()
    {
        if (!self::$fm) {
            self::$fm = new FactoryMuffin(new ModelStore());
            self::$fm->loadFactories(__DIR__.'/factories');
        }
        self::getFactory();

        return self::$fm;
    }

    public static function getFactory()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

class ModelStore extends \League\FactoryMuffin\Stores\ModelStore
{
    protected function save($model)
    {
        $tablename = $model->table;
        $fields = $model->db->field_data($tablename);
        $data = [];
        foreach ($fields as $field) {
            $fName = $field->name;
            if (isset($model->$fName)) {
                $data[$fName] = $model->$fName;
            };
        }
        $result = $model->create($data);
        if (!$result) {
            dump($model->db->error());
            return false;
        }
        $model->{$model->primaryKey} = $result[$model->primaryKey] ?? null;
        return $model;
    }
}
