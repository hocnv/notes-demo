<?php
namespace Jinjer\tests;

/**
 * Phinx outside cli
 */
class MigrationDataHelper
{
    /**
     * @var mixed
     */
    public $CI;

    /**
     * @param $phixConfig
     */
    public function __construct($phixConfig = null)
    {
        $this->CI = &get_instance();
        $phinxApp = new \Phinx\Console\PhinxApplication();
        $this->phinxTextWrapper = new \Phinx\Wrapper\TextWrapper($phinxApp);

        if ($phixConfig == null) {
            $this->phinxTextWrapper->setOption('configuration', APPPATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'phinx.php');
        } else {
            $this->phinxTextWrapper->setOption('configuration', $phixConfig);
        }

        $this->phinxTextWrapper->setOption('parser', 'php');
        $this->phinxTextWrapper->setOption('environment', getenv('environment') ? getenv('environment') : 'development');
    }

    public function clearDatabase()
    {
        $database = $this->CI->db->database;
        $query = $this->CI->db->query('SHOW TABLES');
        $this->exec('SET FOREIGN_KEY_CHECKS=0');
        foreach ($query->result_array() as $row) {
            $table = $row['Tables_in_' . $database];
            $this->exec("DROP TABLE `$table`");
        }
        $this->exec('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * @param array $listTables
     */
    public function truncateDatabase($listTables = [])
    {
        $database = $this->CI->db->database;
        if (empty($listTables)) {
            $query = $this->CI->db->query('SHOW TABLES');
            foreach ($query->result_array() as $row) {
                $table = $row['Tables_in_' . $database];
                if ($table !== 'migrations') {
                    $listTables[] = $table;
                }
            }
        }

        $query = $this->CI->db->query("SELECT DISTINCT `table_name` FROM `information_schema`.`tables` WHERE `table_name` IN ('" . implode("','", $listTables) . "') AND AUTO_INCREMENT > 1");
        $this->exec('SET FOREIGN_KEY_CHECKS=0');
        foreach ($query->result_array() as $table) {
            $this->exec("TRUNCATE `{$table['table_name']}`");
        }
        $this->exec('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * @param  $queries
     * @param  $seperator
     * @return mixed
     */
    public function exec($queries, $seperator = ';')
    {
        if (empty($queries)) {
            return 0;
        }

        if (!is_array($queries)) {
            $queries = explode($seperator, $queries);
        }

        $results = [];
        foreach ($queries as $query) {
            $results[] = $this->CI->db->query($query);
        }
        return $results;
    }

    /**
     * @return mixed
     */
    public function migrate()
    {
        $this->phinxTextWrapper->getMigrate();
        return $this->phinxTextWrapper->getStatus();
    }

    /**
     * @return mixed
     */
    public function rollback()
    {
        $this->phinxTextWrapper->getRollback();
        return $this->phinxTextWrapper->getStatus();
    }

    /**
     * @return mixed
     */
    public function seed()
    {
        $this->phinxTextWrapper->getSeed();
        return $this->phinxTextWrapper->getStatus();
    }

    /**
     * @param  $arr
     * @return mixed
     */
    public function fetchAll($arr)
    {
        $arr2 = [];
        foreach ($arr as $key => $value) {
            foreach ($value as $key => $table) {
                $arr2[] = $table;
            }
        }
        return $arr2;
    }
}
