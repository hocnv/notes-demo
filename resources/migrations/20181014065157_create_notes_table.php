<?php

use Phinx\Migration\AbstractMigration;

class CreateNotesTable extends AbstractMigration
{
    public function up()
    {
        $this->table('notes', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('content', 'text')
            ->addColumn('color', 'string')
            ->addColumn('created', 'datetime')
            ->addColumn('updated', 'datetime')
            ->addColumn('deleted', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->dropTable('notes');
    }
}
