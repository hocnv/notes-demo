/**
 * Created by apple on 10/14/18.
 */

$(document).on('mouseenter','.item_note', function () {
    $(this).find(".edit_note").show();
    $(this).find(".delete_note").show();
}).on('mouseleave','.item_note',  function(){
    $(this).find(".edit_note").hide();
    $(this).find(".delete_note").hide();
});

$(document).on('click','.delete_note', function () {
    var id = $(this).attr('data-id');
    $("#delete_note").find(".delete_input").val(id);
    $("#delete_note").modal('show');
});

$(".btn-delete-confirm").on('click', function() {
    $("#delete_form").submit();
});

$(document).on('click','.edit_note', function () {
    $("#header_popup").html("Update Note");
    var id = $(this).attr('data-id');
    var content = $.trim($(this).parent().find("textarea").val());
    $("#msg_result").html('').attr('class', '');
    var color = $(this).attr('data-color');
    $(".id_note").val(id);
    $(".content_note").val(content);
    $(".color_note").val(color);
    $('#detail_note').modal('show');
});

$(".btn-save-notes").click(function () {
    var content = $(".content_note").val();
    var color = $(".color_note").val();
    var id = $(".id_note").val();
    var action = (id != "") ? "/edit/"+id : "/create";
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: action,
        data: {
            'content' : content,
            'color' : color,
            'csrf_test_name' :  $("#form_notes").find("input[name=csrf_test_name]").val()
        },
        success: function (data) {
            data = JSON.parse(data);
            var msg = data.message;
            if (data.status) {
                msg_class = "form-control alert-success";
                if (id != "") {
                    $(".note_"+id).find('textarea').attr('style', 'background-color:'+color).val(content);
                    $(".note_"+id+" .edit_note").attr("data-color",color);
                } else {
                    var item = $(".item_note").length;
                    var class_note = (is_odd(item)) ? "box_class_odd" : "box_class_even";
                    var new_note = '<div class="col-md-3 item_note note_'+data.id+'">'
                        +'<textarea readonly style="background-color: '+color+'" class="box_note '+class_note+'">'+content+'</textarea>'
                        +'<a class="edit_note" data-color="'+color+'" data-id="'+data.id+'"> <i class="fa fa-edit"></i> </a>'
                        +'<a class="delete_note" data-id="'+data.id+'"> <i class="fa fa-trash"></i> </a></div>';
                    if (item > 0) {
                        $(".note_list").append(new_note);
                    } else {
                        $(".note_list").html(new_note);
                    }
                }
                setTimeout(function () {
                    $('#detail_note').modal('hide');
                }, 2000);
            } else {
                msg_class = "form-control alert-danger";
            }

            $("#msg_result").html(msg).attr('class', msg_class);
            
        }
    });
});

$(".create_note").click(function () {
    $("#header_popup").html("Create Note");
    $("#msg_result").html('').attr('class', '');
    $(".id_note").val('');
    $(".content_note").val('');
    $(".color_note").prop('selectedIndex',0);
    $('#detail_note').modal('show');
});

function is_odd(n) {
    return Math.abs(n % 2) == 1;
}